def run
	x = Sort.new
	zbior = x.input_strings
	puts "wprowadzanie zakończone, sortowanie w toku..."
	x.sort(zbior)
end

class Sort

	def input_strings
		elements = []

		puts 'wprowadź kolejne łańcuchy (pusty oznacza koniec wprowadzania):'

		loop do
			element = STDIN.gets.chop
		
			break if element.empty?
		
			elements << element
		end

		elements
	end

	def sort(element = [])
		fail if element.empty? || element.size < 2

		loop do
			i = 0
			replaced = nil
		
			loop do
				temp = nil
				
				if element[i + 1] < element[i]
					replaced = true
					temp = element[i + 1]
					element[i + 1] = element[i]
					element[i] = temp
				end

				break if i == element.size - 2
				
				i += 1
			end
			
			break unless replaced
		end
		
		element
	rescue
		puts "błąd: zbyt krótki zbiór danych"
	end
end
